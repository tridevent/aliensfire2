﻿using UnityEngine;
using System.Collections;

public class Sonido : MonoBehaviour
{
    public static bool sonido;
    public bool cerradura;
    void Start()
    {
        sonido = true;
    }

    void Update()
    {
        if (sonido == true && cerradura == true)
        {
            GetComponent<AudioSource>().Play();
            cerradura = false;
        }
        if (sonido == false && cerradura == false)
        {
            GetComponent<AudioSource>().Pause();
            cerradura = true;
        }
    }
}