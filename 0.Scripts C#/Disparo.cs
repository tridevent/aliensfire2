﻿using UnityEngine;
using System.Collections;

public class Disparo : MonoBehaviour
{

    public Transform salidadisparo;
    public Transform armaExtra1;
    public Transform armaExtra2;
    public GameObject bala;
    public AudioClip impact;
    public static bool mejora = false;
    public static int restante = 0;
    void Start()
    {
        mejora = false;
        restante = 0;
        InvokeRepeating("disparar", 1, 0.2f);
    }
    void disparar()
    {
        if (CuentaRegresiva.tiempo1 <= 196)
        {
            if (ScPlayer.disparotouch == false && ScPlayer.cerradura == false)
            {
                ScPlayer.disparotouch = true;
                ScPlayer.cerradura = true;
                Instantiate(bala, salidadisparo.position, salidadisparo.rotation);
                if (mejora == true)
                {
                    Instantiate(bala, armaExtra1.position, armaExtra1.rotation);
                    Instantiate(bala, armaExtra2.position, armaExtra2.rotation);
                    restante--;
                    if (restante <= 0)
                    {
                        mejora = false;
                        //restante=15;
                    }
                }
                AudioSource.PlayClipAtPoint(impact, transform.position);
            }
        }
    }

}