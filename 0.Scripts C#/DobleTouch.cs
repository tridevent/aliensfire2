﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DobleTouch : MonoBehaviour
{
    public static int temporizador = 0;
    void Start()
    {
        temporizador = 0;
        InvokeRepeating("Contar", 1, 1);
    }

    void Contar()
    {
        if (temporizador < 3)
        {
            temporizador++;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Update()
    {
        if (temporizador == 0)
        {
             GetComponent<Text>().text = "Cargando...";
        }
    }
}
