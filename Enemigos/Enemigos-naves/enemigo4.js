var vel:int=10;
var comienzo:int=50;
var fin:int=125;
var reset1: Vector3;
var game1:GameObject;
var explosion2:Transform;
var sonidoDisparo:AudioClip;
var disparo:Transform;
var arma1:Transform; 
var contador:int=0;
var vida:int=3;
var textura1:Texture;
var textura2:Texture;
var textura3:Texture;
var tiempo:int=0;
function Start () 
{
tiempo=0;
GetComponent.<Renderer>().material.mainTexture=textura1;
InvokeRepeating("texturizado",1,0.2); 
vida=3;
}
function Update () 
{
if(cuentaregresiva.tiempo1<=196)
	{
	transform.Translate(Vector3(1,0,0)*vel*Time.deltaTime);
	disparoEnemigo();//retardo de como dispara el enemigo
	}
if(transform.position.x<=-5)//reset posicion y desactivamos el enimgo para que vuelva aparecer en otra ocasion 
	{
	reset1= Vector3(Random.Range(comienzo,fin),Random.Range(14,-0.5),0);
	transform.position=reset1;
	}
}
function OnTriggerEnter (other : Collider) {

	if(other.gameObject.tag=="jugador")
	{
///////////////////////////////////////////////////////////////////////////////
		if(cuentaregresiva.tiempo1<=196)
			{
			game1.transform.GetComponent(ScGameState1).restavidas5();
			}
////////////////////////////////////////////////////////////////////////////////
		if(ScGameState1.graficosP==true){
	    	Instantiate (explosion2,transform.position,transform.rotation);
	    }
	    reset1= Vector3(Random.Range(comienzo,fin),Random.Range(14,-0.5),0);
	    GetComponent.<AudioSource>().PlayClipAtPoint(sonidoDisparo,transform.position);
	    transform.position=reset1;
  }
  if(other.gameObject.tag=="disparo" || other.gameObject.tag=="escudo" || other.gameObject.tag=="misilobj")
	{
///////////////////////////////////////////////////////////////////////////////
		if(cuentaregresiva.tiempo1<=196)
			{
			game1.transform.GetComponent(ScGameState1).AddScore2();
			}
////////////////////////////////////////////////////////////////////////////////
		if(ScGameState1.graficosP==true){
	    	Instantiate (explosion2,transform.position,transform.rotation);
	    }
	vida--;
	if(vida==0)
		{
	    reset1= Vector3(Random.Range(comienzo,fin),Random.Range(14,-0.5),0);
	    GetComponent.<AudioSource>().PlayClipAtPoint(sonidoDisparo,transform.position);
	    transform.position=reset1;
	    vida=3;
	    }
  }
}
function disparoEnemigo()
{
contador++;
if(contador==150)
	{
	Instantiate (disparo,arma1.position,disparo.rotation);
	contador=0;
	}
}
function texturizado()
{
tiempo++;
if(tiempo==1)
	{
	GetComponent.<Renderer>().material.mainTexture=textura1;
	}
if(tiempo==2)
	{
	GetComponent.<Renderer>().material.mainTexture=textura2;
	}
if(tiempo==3)
	{
	GetComponent.<Renderer>().material.mainTexture=textura3;
	tiempo=0;
	}
}
