var vel:int=10;
var comienzo:int=50;
var fin:int=125;
var reset1: Vector3;
var game1:GameObject;
var explosion2:Transform;
var sonidoDisparo:AudioClip;
//////escudo/////////////////
static var escudo:boolean;
var textCerrado:Texture;
var textAbierto:Texture;
var contAnimacion:int=0;
var contEscudo:int=0;
var check:boolean=false;
var copia:int;

function Start () 
{
escudo=true;
InvokeRepeating("Escudo",1,1);
}
function Update () 
{
if(congelamientoGUI.congelamientoAct==true)
	{
	if(check==false)
		{
		copia=ScGameState1.timer;
		check=true;
		}
	if((copia-ScGameState1.timer)==30)
		{
		congelamientoGUI.congelamientoAct=false;
		}
	}

/////////////////////////////////////////////////////////////////
		if(cuentaregresiva.tiempo1<=196)
		{
		if(transform.position.y>=ScRoca1.inicio)
			{
			transform.position.y=ScRoca1.inicio;
			}
			if(transform.position.y<=ScRoca1.fin2)
			{
			transform.position.y=ScRoca1.fin2;
			}
		if(ScGameState1.timer>=0 && ScGameState1.timer<=80)
		{		
			if(congelamientoGUI.congelamientoAct==true)
				{	
				transform.Translate(Vector3(1,0,0)*ScRoca1.rocvel2*Time.deltaTime);
				}	
			if(congelamientoGUI.congelamientoAct==false)	
				{
				transform.Translate(Vector3(1,0,0)*ScRoca1.rocvel1*Time.deltaTime);
				}
		}
		if(ScGameState1.timer>80 && ScGameState1.timer<=140)
		{
			if(congelamientoGUI.congelamientoAct==true)
				{	
				transform.Translate(Vector3(1,0,0)*ScRoca1.rocvel3*Time.deltaTime);
				}	
			if(congelamientoGUI.congelamientoAct==false)	
				{
				transform.Translate(Vector3(1,0,0)*ScRoca1.rocvel2*Time.deltaTime);
				}
		}
		if(ScGameState1.timer>140 && ScGameState1.timer<=200)
		{	
			if(congelamientoGUI.congelamientoAct==true)
				{	
				transform.Translate(Vector3(1,0,0)*ScRoca1.rocvel4*Time.deltaTime);
				}	
			if(congelamientoGUI.congelamientoAct==false)	
				{
				transform.Translate(Vector3(1,0,0)*ScRoca1.rocvel3*Time.deltaTime);
				}
		}
		//resetamos la posicion de las rocas
		if(transform.position.x<=-5)
			{
			//creamos una roca aletoriamente
			reset1= Vector3 (Random.Range(ScRoca1.comienzo,ScRoca1.fin),Random.Range(ScRoca1.inicio,ScRoca1.fin2),0);
		    transform.position=reset1;
		   	}
		}
}
function OnTriggerEnter (other : Collider) {

	if(other.gameObject.tag=="jugador")
	{
///////////////////////////////////////////////////////////////////////////////
		if(cuentaregresiva.tiempo1<=196)
			{
			game1.transform.GetComponent(ScGameState1).restavidas5();
			}
////////////////////////////////////////////////////////////////////////////////
		if(ScGameState1.graficosP==true){
	    	Instantiate (explosion2,transform.position,transform.rotation);
	    }
	    reset1= Vector3(Random.Range(comienzo,fin),Random.Range(14,-0.5),0);
	    GetComponent.<AudioSource>().PlayClipAtPoint(sonidoDisparo,transform.position);
	    transform.position=reset1;
  }
  if((other.gameObject.tag=="disparo" && escudo==false) || other.gameObject.tag=="escudo" || other.gameObject.tag=="misilobj")
	{
///////////////////////////////////////////////////////////////////////////////
		if(cuentaregresiva.tiempo1<=196)
			{
			game1.transform.GetComponent(ScGameState1).AddScore2();
			}
////////////////////////////////////////////////////////////////////////////////
		if(ScGameState1.graficosP==true){
	    	Instantiate (explosion2,transform.position,transform.rotation);
	    }
	    reset1= Vector3(Random.Range(comienzo,fin),Random.Range(14,-0.5),0);
	    GetComponent.<AudioSource>().PlayClipAtPoint(sonidoDisparo,transform.position);
	    transform.position=reset1;
  }
}
function Escudo ()
{
	contEscudo++;
	if(contEscudo>=15)
		{
		escudo=!escudo;
		contEscudo=0;
		}
	if(escudo==true)
		{
		GetComponent.<Renderer>().material.mainTexture=textCerrado;
		}
	if(escudo==false)
		{
		GetComponent.<Renderer>().material.mainTexture=textAbierto;
		}
}
