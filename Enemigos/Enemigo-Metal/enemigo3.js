var vel:int=10;
var comienzo:int=50;
var fin:int=125;
var reset1: Vector3;
var game1:GameObject;
var explosion2:Transform;
var sonidoDisparo:AudioClip;
var disparo:Transform;
var arma1:Transform; 
var contador:int=0;
//////escudo/////////////////
static var escudo:boolean;
var textCerrado:Texture;
var textAbierto:Texture;
var contAnimacion:int=0;
var contEscudo:int=0;
var i:int=0;
var check:boolean=false;
var copia:int;

function Start () 
{
escudo=true;
InvokeRepeating("Escudo",1,1);
}
function Update () 
{
if(congelamientoGUI.congelamientoAct==true)
	{
	if(check==false)
		{
		copia=ScGameState1.timer;
		check=true;
		}
	if((copia-ScGameState1.timer)==30)
		{
		congelamientoGUI.congelamientoAct=false;
		}
	}
/////////////////////////////////////////////////////////////////
		if(cuentaregresiva.tiempo1<=196)
		{
		transform.Translate(Vector3(1,0,0)*vel*Time.deltaTime);
		disparoEnemigo();
		}
		//resetamos la posicion de las rocas
		if(transform.position.x<=-5)
			{
			//creamos una roca aletoriamente
			reset1= Vector3 (Random.Range(ScRoca1.comienzo,ScRoca1.fin),Random.Range(ScRoca1.inicio,ScRoca1.fin2),0);
		    transform.position=reset1;
		   	}
		
}
function OnTriggerEnter (other : Collider) {

	if(other.gameObject.tag=="jugador")
	{
///////////////////////////////////////////////////////////////////////////////
		if(cuentaregresiva.tiempo1<=196)
			{
			game1.transform.GetComponent(ScGameState1).restavidas5();
			}
////////////////////////////////////////////////////////////////////////////////
		if(ScGameState1.graficosP==true){
	    	Instantiate (explosion2,transform.position,transform.rotation);
	    }
	    reset1= Vector3(Random.Range(comienzo,fin),Random.Range(14,-0.5),0);
	    GetComponent.<AudioSource>().PlayClipAtPoint(sonidoDisparo,transform.position);
	    transform.position=reset1;
  }
  if((other.gameObject.tag=="disparo" && escudo==false) || other.gameObject.tag=="escudo" || other.gameObject.tag=="misilobj")
	{
///////////////////////////////////////////////////////////////////////////////
		if(cuentaregresiva.tiempo1<=196)
			{
			game1.transform.GetComponent(ScGameState1).AddScore2();
			}
////////////////////////////////////////////////////////////////////////////////
		if(ScGameState1.graficosP==true){
	    	Instantiate (explosion2,transform.position,transform.rotation);
	    }
	    reset1= Vector3(Random.Range(comienzo,fin),Random.Range(14,-0.5),0);
	    GetComponent.<AudioSource>().PlayClipAtPoint(sonidoDisparo,transform.position);
	    transform.position=reset1;
  }
 if(other.gameObject.tag=="disparo" && escudo==true)
	{
		if(ScGameState1.graficosP==true){
	    	Instantiate (explosion2,transform.position,transform.rotation);
	    }
	    GetComponent.<AudioSource>().PlayClipAtPoint(sonidoDisparo,transform.position);
  }
}
function Escudo ()
{
	contEscudo++;
	if(contEscudo>=15)
		{
		escudo=!escudo;
		contEscudo=0;
		}
	if(escudo==true)
		{
		GetComponent.<Renderer>().material.mainTexture=textCerrado;
		}
	if(escudo==false)
		{
		GetComponent.<Renderer>().material.mainTexture=textAbierto;
		}
}


function disparoEnemigo()
{
	contador++;
	if(contador==150)
		{
		Instantiate (disparo,arma1.position,disparo.rotation);
		contador=0;
		}
}

